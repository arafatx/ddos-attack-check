#!/bin/sh
#ddos-check.sh - DDOS Attack Check
#By Arafat Ali
#Version 1.0

#Global Variables
MYFILENAME="ddos-report.txt"
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="OK"
#ACTIVE_CONN_IP_COUNT_80=`netstat -an | grep :80 | wc -l`
ACTIVE_SYNC_IP_COUNT_80=`netstat -n -p | grep :80 | grep SYN | wc -l`
ACTIVE_CONN_IP_COUNT_80_443=`netstat -an | egrep ":80|:443" | wc -l`
ACTIVE_SYNC_IP_COUNT_ALL_PORTS=`netstat -n -p | grep SYN | wc -l`
ACTIVE_SYNC_IP_COUNT_80_443=`netstat -n -p | egrep ":80|:443" | grep SYN | wc -l`
MIN_TRUSTED_SYNC_IP=5
MAX_TRUSTED_SYNC_IP=15
MIN_TRUSTED_ACTIVE_IP=10
MAX_TRUSTED_ACTIVE_IP=100
#echo "ACTIVE_COON_IP: $ACTIVE_CONN_IP_COUNT_80"
#echo "ACTIVE_SYNC_REC_COUNT: $ACTIVE_SYNC_REC_COUNT_80"
#ACTIVE_CONN_IP_80=`netstat -an | grep :80 | sort`
#IP_LOGGED_COUNT_LIST=`netstat -anp |grep 'tcp\|udp' | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n`

/bin/echo "======================================"
/bin/echo "[ddos] DDOS Attack is checking system..."
/bin/echo "DDOS Attack checked on `date`" > /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "=======================================">> /tmp/$MYFILENAME
/bin/echo "Count active SYNC_REC on port 80 (POSSIBLE DDOS ATTACK):" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -n -p | egrep ":80" | grep SYN | wc -l >> /tmp/$MYFILENAME
/bin/echo "MAX DEFINED VALUE:  $MAX_TRUSTED_SYNC_IP" >> /tmp/$MYFILENAME
if [ "${ACTIVE_SYNC_IP_COUNT_80}" -le $MIN_TRUSTED_SYNC_IP ]; then
/bin/echo "OK: ACTIVE SYNC IP COUNT IS OK" >> /tmp/$MYFILENAME
elif [[ "${ACTIVE_SYNC_IP_COUNT_80}" -ge $MIN_TRUSTED_SYNC_IP && "${ACTIVE_SYNC_IP_COUNT_80}" -lt $MAX_TRUSTED_SYNC_IP ]]; then
WARNING_STATUS="SUGGESTION"
/bin/echo "SUGGESTION: THE PREFERABLY NUMBER SHOULD BE LESS THAN $MIN_TRUSTED_SYNC_IP. AUDIT SYNC IPs TO FIND ATTACKERS" >> /tmp/$MYFILENAME
elif [ "${ACTIVE_SYNC_IP_COUNT_80}" -ge $MAX_TRUSTED_SYNC_IP ]; then
/bin/echo "WARNING: THE NUMBER INDICATES THAT YOU MIGHT HAVE SYNC ATTACK ON $MYHOSTNAME. AUDIT THE IPs IMMEDIATELY!" >> /tmp/$MYFILENAME
WARNING_STATUS="WARNING"
fi
/bin/echo "" >> /tmp/$MYFILENAME
/bin/echo "List of active IP(s) connection with SYNC listening status on port 80 & 443:" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -n -p | egrep ":80|:443" | grep SYN | sort -u >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "List of active IP(s) connection with SYNC listening status NOT on ports 80 & 443:" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -n -p | egrep -v ":80|:443"  | grep SYN | sort -u >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "=======================================">> /tmp/$MYFILENAME
/bin/echo "Count all active IPs on ports 80 & 443:" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -an | egrep ":80|:443" | wc -l >> /tmp/$MYFILENAME
/bin/echo "MAX DEFINED VALUE:  $MAX_TRUSTED_ACTIVE_IP" >> /tmp/$MYFILENAME
if [ "${ACTIVE_CONN_IP_COUNT_80_443}" -lt $MIN_TRUSTED_ACTIVE_IP ]; then
/bin/echo "OK: MIGHT BE OK" >> /tmp/$MYFILENAME
elif [[ "${ACTIVE_CONN_IP_COUNT_80_443}" -ge $MIN_TRUSTED_ACTIVE_IP &&  "${ACTIVE_CONN_IP_COUNT_80_443}" -lt $MAX_TRUSTED_ACTIVE_IP ]]; then
#WARNING_STATUS="SUGGESTION"
/bin/echo "SUGGESTION: THE PREFERABLY NUMBER SHOULD BE LESS THAN $MIN_TRUSTED_ACTIVE_IP. AUDIT IPs TO FIND ATTACKERS" >> /tmp/$MYFILENAME
elif [ "${ACTIVE_CONN_IP_COUNT_80_443}" -ge $MAX_TRUSTED_ACTIVE_IP ]; then
#WARNING_STATUS="WARNING"
/bin/echo "WARNING: THERE ARE TOO MANY ACTIVE CONNECTIONS ON PORT 80: ( $ACTIVE_CONN_IP_COUNT_80_443 CONNECTIONS FOUND ). AUDIT THE IPs IMMEDIATELY!" >> /tmp/$MYFILENAME
fi
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "List of active IPs on port 80 & 443 with listening status" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -an | egrep ":80|:443" | sort >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "List of frequent IP(s) logged into server from tcp/udp ports:" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -anp |grep 'tcp\|udp' | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "List of frequent IP(s) logged into server from tcp/udp ports:" >> /tmp/$MYFILENAME
/bin/echo "=======================================">> /tmp/$MYFILENAME
netstat -anp |grep 'tcp\|udp' | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "=======================================">> /tmp/$MYFILENAME
if [ "${WARNING_STATUS}" == "WARNING" ]; then
#/bin/mail -s "[ddos][$WARNING_STATUS|CAIP=$ACTIVE_CONN_IP_COUNT_80_443/$MAX_TRUSTED_ACTIVE_IP|CSIP=$ACTIVE_SYNC_IP_COUNT_80_443/$MAX_TRUSTED_SYNC_IP] DDOS Attack Scan Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$MYFILENAME
/bin/mail -s "[ddos][$WARNING_STATUS|CSIP=$ACTIVE_SYNC_IP_COUNT_80/$MAX_TRUSTED_SYNC_IP] DDOS Attack Scan Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$MYFILENAME
fi
/bin/rm -rf /tmp/$MYFILENAME

/bin/echo "[ddos] Scan status: $WARNING_STATUS"
/bin/echo "[ddos] Done checking system. Email is set to be sent to $MYEMAIL"
/bin/echo "================================="